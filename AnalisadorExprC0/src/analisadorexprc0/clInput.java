/**
 *
 * @author Fernando A. Damião <me@fadamiao.com>
 * @license BSD 3-Clause License
 * 
 */

package analisadorexprc0;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class clInput
{
    private BufferedReader kdb;

    public clInput()
    {
        kdb = new BufferedReader(new InputStreamReader(System.in));
    }

    public String readString()
    {
        try {
            String s = kdb.readLine();
            return s;
        } catch(IOException e) {
            System.out.println("Error: " + e);
            return "";
        }
    }
}
