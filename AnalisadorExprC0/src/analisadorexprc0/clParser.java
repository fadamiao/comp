/**
 *
 * @author Fernando A. Damião <me@fadamiao.com>
 * @license BSD 3-Clause License
 * 
 */

package analisadorexprc0;

import java.util.*;

public class clParser
{
    private clToken token;
    private clAnalex analex;
    private String translate, command, expr;
    private int IP, indID;
    private Hashtable<String, clToken> Table;

    public clParser(clInput x)
    {
        analex = new clAnalex(x);
        token = analex.getToken();
        translate = "";
        command = "";
        expr = "";
        Table = new Hashtable();
        IP = 0;
        indID = 0;
    }

    private int newAddress()
    {
        return indID++;
    }

    private boolean showError(String s)
    {
        System.out.println(s);
        return false;
    }

    public void analyze()
    {
        boolean k;
        k = program();

        if (k) {
            System.out.println("Correct Program");
            System.out.println(translate);
        } else {
            System.out.println("Compilation Error");
        }
    }

    private boolean program()
    {
        boolean k;
        System.out.println("Program...");

        translate = "";
        k = command();
        translate = translate + IP + " " + command + "\n";
        IP++;

        while (!token.end() && k) {
            if (token.equal(";")) {
                match(";");
            } else {
                return showError("Error: ';' expected!");
            }

            if (!token.end()) {
                k = command();
                translate = translate + IP + " " + command + "\n";
                IP++;
            }
        }
        return true;
    }

    private boolean command()
    {
        boolean k;
        System.out.println("Command..." + token);

        command = "";
        if (token.equal("read")) {
            k = read();
            return k;
        }
        if (token.equal("write")) {
            System.out.println("Calling write");
            k = write();
            return k;
        }
        if (token.equal("halt")) {
            command = "halt";
            match("halt");
            return true;
        }
        k = attrib();
        return k;
    }

    private boolean read()
    {
       clToken w; 
       Integer addr;

       command = "set ";
       match("read");

        if (!match("(")) {
            return showError("Error: '(' expected!");
        }

        if (token.equal("id")) {
            w = Table.get(token.value);
            // Se não estiver, insere e cria um endereço na memoria para ele.
            if (w == null) {
                addr = newAddress();
                token.info = addr.toString();
                Table.put(token.value, token);
                command += token.info;
            } else {
               command += w.info;
            }
            match("id");
            command = command + ", read";
        } else {
            return false;
        }

        if (!match(")")) {
            return showError("Error: ')' expected!");
        }

        return true;
    }

    private boolean write()
    {
        System.out.println("Write...");

        command = "set write, ";
        match("write");
        if (!match("(")) {
            return showError("Error: '(' expected!");
        }

        expr = "";
        if (!Expr()) {
            return showError("Expression error!");
        }

        command += expr;
        expr = "";
        if (!match(")")) {
            return showError("Error: ')' expected!");
        }

        return true;
    }

    private boolean attrib()
    {
        clToken w;
        Integer addr;
        System.out.println("Atrib...");

        if (token.equal("id")) {
            // Verifica se o identificador já está na tabela.
            w = Table.get(token.value);
            if (w == null) { // Se não estiver, insere e cria um endereço na memoria para ele.
                System.out.println("New variable in " + indID);
                addr = newAddress();
                token.info = addr.toString();
                Table.put(token.value, token); 
                command = "set " + token.info + ",";
            } else {
                command = "set " + w.info + ",";
            }
            match("id");
        } else {
            return showError("Identifier expected!");
        }

        if (token.equal("=")) {        
            match("=");
        } else {
            return showError("Error: '=' expected!");
        }

        expr="";

        if (Expr()) {
            command = command + expr;
            expr="";
        } else {
            return showError("Expression expected!");
        }
        return true;
    }

    private boolean Expr()
    {
        if (!Term()) {
            return false;        
        }
        if (!R()) {
            return false;
        }
        return true;
    }

    private boolean R()
    {
        if (token.equal("+")) {
            match("+");
            expr += "+";
            if (!Term()) {
                return showError("Expression Error: 3");
            }
            if (!R()) {
                return false;
            }
            return true;
        }
        if (token.equal("-")) {
            match("-");
            expr += "-";
            if (!Term()) {
                return showError("Expression Error: 3");
            }
            if (!R()) {
                return false;
            }
            return true;
        }
        return true;
    }

    private boolean Term()
    {
        if (!Factor()) {
            return showError("Expression Error: 2");
        }
        if (!Z()) {
            return false;
        }
        return true;
    }

    private boolean Z()
    {
        if (token.equal("*")) {
            match("*");
            expr += "*";
            if (!Factor()) {
                return showError("Expression Error: 1");
            }
            if (!Z()) {
                return false;
            }
            return true;
        }
        if (token.equal("/")) {
            match("/");
            expr += "/";
            if (!Factor()) {
                return showError("Expression Error: 1");
            }
            if (!Z()) {
                return false;
            }
            return true;
        }
        return true;
    }

    private boolean Factor()
    {
        clToken w;
        System.out.println(token);

        if (token.equal("id")) {
            w = Table.get(token.value);
            if (w == null) { // Se não estiver, insere e cria um endereço na memoria para ele.
               return showError("Identifier " + token.value + " unknown");
            } else {
               expr += "D[" + w.info + "]";
            }
            match("id");
            return true;
        }
        if (token.equal("(")) {
            match("(");
            expr += "(";
            Expr();
            if (match(")")) {
                expr += ")";
            } else {
                return showError("Error: ')' expected!");
            }
            return true;
        }
        if (token.equal("num")) {
            expr += token.value;
            System.out.println("here");
            match("num");
            return true;
        } 

        System.out.println("Syntax Error: 1");
        return false;
    }

    private boolean match(String s)
    {
        if (token.equal(s)) {
            token = analex.getToken();
            return true;
        } else {
            return showError("Syntax Error: 3");
        }
    }    
}
