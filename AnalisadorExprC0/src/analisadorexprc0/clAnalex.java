/**
 *
 * @author Fernando A. Damião <me@fadamiao.com>
 * @license BSD 3-Clause License
 * 
 */

package analisadorexprc0;

public class clAnalex
{
    private char next, auxnext;
    private clInput input;
    private int line, p;

    public clAnalex(clInput in)
    {
        input = in;
        line = 0;
        p = 0;
        next = ' ';
        auxnext = ' ';
    }

    private boolean isDigit(char c)
    {
        return (c >= '0') && (c <= '9');
    }

    private boolean isLetter(char c)
    {
        return ((c >= 'a') && (c <= 'z')) || ((c >= 'A') && (c <= 'Z'));
    }

    private boolean isBlank(char c)
    {
        return ((c == ' ') || (c == '\t')) || ((c == '\n') || (c == '\r')) ;
    }

    private char following()
    {
        int v;
        char x, c;
        if (auxnext != ' ') {
            x = auxnext;
            auxnext = ' ';
            return x;
        }
        c = input.();
        return c;
    }

    public clToken getToken()
    {
        int v;
        String s = "";
        clToken w;

        // Eliminate blanks
        if (isBlank(next)) {
            for (;;next = following()) {
                if (isBlank(next)) {
                    if (next == '\n') {
                        line++;
                    } else {
                        break;
                    }
                }
            }
        }

        if (next == (char) 0) {
            return new clToken("end", "end");
        }

        // Process numbers
        if (isDigit(next)) {
            v = 0;
            do {
                v = v * 10 + (next - '0');
                next = following();
            } while (isDigit(next));

            return new clToken("num", v + "");
        }

        // Process identifiers and reserved words
        if (isLetter(next)) {
            do {
                s = s + next;
                next = following();
            } while (isLetter(next) || isDigit(next));

            if (s.equals("read")) {
                return new clToken("read", s);
            }

            if (s.equals("write")) {
                return new clToken("write", s);
            }

            if (s.equals("halt")) {
                return new clToken("halt", s);
            }

            return new clToken("id", s);
        }

        w = new clToken(next + "", next + "");
        next = ' ';
        return w;
    }
}
