/**
 *
 * @author Fernando A. Damião <me@fadamiao.com>
 * @license BSD 3-Clause License
 * 
 */

package analisadorexprc0;

public class clToken
{
    String type, value, info;

    public clToken(String t, String v)
    {
        type = t;
        value = v;
    }

    public clToken(String t, String v, String i)
    {
        type = t;
        value = v;
        info = i;
    }
    
    public boolean equal(String s)
    {
        return type.equals(s);
    }
    
    public String toString()
    {
        return type + ":" + value;
    }

    public String info()
    {
        return info;
    }

    public boolean end()
    {
        return type.equals("end");
    }
}
