/**
 *
 * @author Fernando A. Damião <me@fadamiao.com>
 * @license BSD 3-Clause License
 * 
 */

package analisadorexprc0;

public class AnalisadorExprC0
{
    public static void main(String[] args)
    {
        clInput in = new clInput();
        String s = in.readString();

        clParser p = new clParser(s);
        String res = p.analyze();

        System.out.println("Result: " + res);
    }
}
