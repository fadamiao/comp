package analizadorexpr2;

import java.util.*;

/*
E ::= T R
R ::= +T R | ""
T ::= F Z 
Z ::= *F Z | "" 
F ::= a | b | (E) ->  id | (E) | numero
*/

public class clParser
{
    private CToken tok;
    private String traducao;
    private clAnalex analex;
    private String comando;
    private int IP;
    private Hashtable<String,CToken> Tabela;
    private int indID;
    private String expr;
   
    public clParser(clEntrada x)
    {
        analex = new clAnalex(x);
        tok = analex.pegaToken(); 
        traducao = "";
        comando = "";
        expr = "";
        Tabela = new Hashtable<String,CToken>();
        IP = 0;
        indID = 0;
    }
   
    private int novo_endereco()
    {
        return indID++;
    }
  
    public String analisa()
    {
        Boolean k;
        k = programa();
        if (k) {
            System.out.println("Programa correto");
            System.out.println(traducao);
            return traducao;
        } else {
            System.out.println("Erro de compilação");
            return null;
        }
    }
   
    private boolean Erro(String msg)
    {
        System.out.println("Erro: " + msg);
        return false;
    }
   
    private boolean programa()
    {
        boolean k;
        System.out.println("Programa...");

        if (match("main") && match("(") && match(")")) {
            if (!Bloco()) {
                return Erro("Bloco esperado!");
            }
        }
        return true;
    }

    private boolean comando()
    {
        boolean k;
        System.out.println("Comando..." + tok);
        comando = "";

        if (tok.igual("read")) {
            k = leitura();
            if (tok.igual(";")) {
                match(";");
            } else {
                return Erro("; esperado");
            }
            return k;
        }
        if (tok.igual("write")) {
            System.out.println("chamou escrita");
            k = escrita();
            if (tok.igual(";")) {
                match(";");
            } else {
                return Erro("; esperado");
            }
            return k;
        }
        if (tok.igual("halt")) {
            comando = "halt";
            match("halt");
            if (tok.igual(";")) {
                match(";");
            } else {
                return Erro("; esperado");
            }
            return true;
        }
        if (tok.igual("if")) {
            IF();
        }
        if (tok.igual("for")) {
            FOR();
        }
        if (tok.igual("while")) {
            WHILE();
        }
        if (tok.igual("int")) {
            match("int");
            decl();
            if (tok.igual(";")) {
                match(";");
            } else {
                return Erro("; esperado");
            }
            return true;
        }

        k = atrib();
        if (tok.igual(";")) {
            match(";");
        } else {
            return Erro("; esperado");
        }
        return k;
    }

    private boolean leitura()
    {
        CToken w; 
        Integer endereco;

        comando = "set ";
        match("read");

        if (!match("(")) {
            return Erro("'(' esperado.");
        }

        if (tok.igual("id")) {
            w = Tabela.get(tok.val);
            if (w == null) { // Se não estiver, insere e cria um endereço na memoria para ele.
                endereco = novo_endereco();
                tok.info = endereco.toString();
                Tabela.put(tok.val, tok);
                comando += tok.info;
            } else {
                comando += w.info;
            }
            match("id");
            comando = comando + ", read";
        } else {
            return false;
        }

        if (!match(")")) {
            return Erro("')' esperado.");
        }

        return true;
    }

    private boolean escrita()
    {
        System.out.println("Escrita...");
        comando = "set write,";
        match("write");

        if (!match("(")) {
            return Erro("'(' esperado.");
        }

        expr = "";
        if (!Expr()) {
            return Erro("Erro na experessão");
        }

        comando += expr;
        expr = "";
        if (!match(")")) {
            return Erro("')' esperado.");
        }

        return true;
    }
   
    private boolean decl()
    {
        CToken w;
        Integer endereco;

        if (tok.igual("id")) {
            w = Tabela.get(tok.val);
            if (w == null) {
                System.out.println("Nova variavel em " + indID);
                endereco = novo_endereco();
                tok.info = endereco.toString();
                Tabela.put(tok.val, tok);
                match("id");
            } else {
                return Erro("Já existe");
            }
        } else {
            return Erro("Identificador esperado");
        }

        return true;
    }
   
    private boolean atrib()
    {
        System.out.println("Atrib...");
        CToken w;
        Integer endereco;

        if (tok.igual("id")) {
            // Verifica se o identificador já está na tabela.
            w = Tabela.get(tok.val);
            if (w == null) { 
                return Erro("identificador não encontrado");
            } else {
                comando = "set " + w.info + ",";
            }
            match("id");
        } else {
           return Erro("Identificador esperado!");
        }

        if (tok.igual("=")) {
            match("=");
        } else {
            return Erro("'=' esperado.");
        }
  
        expr = "";
        if (Expr()) {
            comando = comando + expr;
            expr="";
        } else {
            return Erro("Expressão esperada!");
        }

        return true;
    }
   
    private boolean Bloco()
    {
        if (tok.igual("{")) {
            match("{");
            while (comando()) {
                if (!comando.equals("")) {
                    traducao = traducao + IP + " " + comando + "\n";
                    IP++;
                }
                if (tok.igual("}")) {
                    break;
                }
            }
            if (!tok.igual("}")) {
                return Erro("} esperado");
            }
            try {
                match("}");
            }
            catch(Exception e) {}
        } else {
            if (!comando()) {
                return Erro("Comando esperado");
            }
        }
       
       return true;
    }

    public boolean Expr()
    {
        if (!Termo()) {
            return false;
        }
        if (!R()) {
            return false;
        }
        return true;
    }

    public boolean logExpr()
    {
        if (!Termo()) {
            return false;
        }
        if (!L()) {
            return false;
        }

        return true;
    }

    public boolean R()
    {
        if (tok.igual("+")) {
            match("+");
            expr += "+";
            if (!Termo()) {
                return Erro("Erro na expressão 3");
            }
            if (!R()) {
                return false;
            }
            return true;
        }
        if (tok.igual("-")) {
            match("-");
            expr += "-";
            if (!Termo()) {
                return Erro("Erro na expressão 3");
            }
            if (!R()) {
                return false;
            }
            return true;
        }
        return true;
    }

    public boolean L() {
        if (tok.igual("=")) {
            match("=");
            if (tok.igual("=")) {
                match("=");
                expr += "==";
                if (!Termo()) {
                    return Erro("Erro");
                }
                if (!L()) {
                    return false;
                }
                return true;
            } else {
                return Erro("Comparação esperada");
            }
        }
        if (tok.igual(">")) {
            match(">");
            if (tok.igual("=")) {
                match("=");
                expr += ">=";
                if (!Termo()) {
                    return Erro("Erro");
                }
                if (!L()) {
                    return false;
                }
            } else {
                expr += ">";
            }
            if (!Termo()) {
                return Erro("Erro");
            }
            if (!L()) {
                return false;
            }
        }
        if (tok.igual("<")) {
            match("<");
            if (tok.igual("=")) {
                match("=");
                expr += "<=";
                if (!Termo()) {
                    return Erro("Erro");
                }
                if (!L()) {
                    return false;
                }
            } else {
                expr += "<";
            }
            if (!Termo()) {
                return Erro("Erro");
            }
            if (!L()) {
                return false;
            }
        }
        return true;
    }

    public boolean Termo()
    {
        if (!Fator()) {
            return Erro("Erro na expressão 2");
        }
        if (!Z()) {
            return false;
        }
        return true;
    }

    public boolean Z()
    {
        if (tok.igual("*")) {
            match("*");
            expr += "*";
            if (!Fator()) {
                return Erro("Erro na expressão 1");
            }
            if (!Z()) {
                return false;
            }
            return true;
        }
        if (tok.igual("/")) {
            match("/");
            expr += "/";
            if (!Fator()) {
                return Erro("Erro na expressão 1");
            }
            if (!Z()) {
                return false;
            }
            return true;
        }
        return true;
    }

    public boolean Fator()
    {
        CToken w;
        System.out.println(tok);

        if (tok.igual("id")) {
            w = Tabela.get(tok.val);
            if (w == null) { // Se não estiver, insere e cria um endereço na memoria para ele.
                return Erro("Identificador " + tok.val + " desconhecido");
            } else {
                expr += "D[" + w.info + "]";
            }
            match("id");
            return true;
        }
        if (tok.igual("(")) {
            match("(");
            expr += "(";
            Expr();
            if (tok.igual(")")) {
                match(")");
                expr += ")";
            } else {
                return Erro("')' esperado.");
            }
            return true;
        }
        if (tok.igual("numero")) {
            expr += tok.val;
            System.out.println("aqui");
            match("numero");
            return true;
        } 

        System.out.println("Erro de sintaxe (2)");
        return false;
    }

    public boolean match(String x)
    {
        if (tok.igual(x)) {
            tok = analex.pegaToken();
            return true;
        } else {
            return Erro("Erro de sintaxe (3)");
        }
    }

    private boolean IF()
    {
        match("if");
        expr = "";
        if (!tok.igual("(")) {
            return Erro("( esperado");
        }
        match("(");
        if (!logExpr()) {
            return Erro("expressão esperada");
        }
        String cond = expr;       
        if (!tok.igual(")")) {
            return Erro(") esperado");
        }
        match(")");
        String aux = traducao;
        int p_IP = IP;
        IP++;
        traducao = "";
        if (!Bloco()) {
            return Erro("Bloco esperado");
        }
        if (match("else")) {
            if (!Bloco()) {
                return Erro("Bloco esperado");
            }
        }

        traducao = aux + p_IP + " jumpt " + IP + ", !" + cond + "\n" + traducao;

        return true;
    }

    private boolean FOR()
    {
        match("for");
        int retr_point = IP;
        expr = "";
        if (!tok.igual("(")) {
            return Erro("( esperado");
        }
        match("(");
        comando = "";
        atrib();
        traducao += IP + " " + comando + "\n";
        IP++;
        if (!tok.igual(";")) {
            return Erro("; esperado");
        }
        match(";");
        if (!logExpr()) {
            return Erro("expressão esperada");
        }
        if (!tok.igual(";")) {
            return Erro("; esperado");
        }
        match(";");
        String cond = expr;
        String aux = traducao;
        int p_IP = IP;
        IP++;
        traducao = "";
        comando = "";
        atrib();
        traducao += IP + " " + comando + "\n";
        IP++;
        if (!tok.igual(")")) {
            return Erro(") esperado");
        }
        match(")");
       
        if (!Bloco()) {
            return Erro("Bloco esperado");
        }
        traducao = aux + p_IP + " jumpt " + (IP + 1) + ", !" + cond + "\n" + traducao + IP + " jump " + p_IP + "\n";
        IP++;
        return true;
    }

    private boolean WHILE()
    {
        match("while");
        int retr_point = IP;
        expr = "";
        if (!tok.igual("(")) {
            return Erro("( esperado");
        }
        match("(");
        if (!logExpr()) {
            return Erro("expressão esperada");
        }
        String cond = expr;
        String aux = traducao;
        int p_IP = IP;
        IP++;
        traducao = "";
        if (!tok.igual(")")) {
            return Erro(") esperado");
        }
        match(")");
        if (!Bloco()) {
            return Erro("Bloco esperado");
        }

        traducao = aux + p_IP + " jumpt " + (IP + 1) + ", !" + cond + "\n" + traducao + IP + " jump " + p_IP + "\n";
        IP++;

        return true;
    }
}
