package analizadorexpr2;

public class clAnalex
{
    private char prox;
    private char auxprox;
    private clEntrada in;
    private int linha;
    private int p;

    public clAnalex(clEntrada x)
    {
        in = x;
        linha = 0;
        p = 0;
        auxprox = ' ';
        prox = ' ';
    }

    private boolean is_digit(char c)
    {
        return (c >= '0') && (c <= '9');
    }

    private boolean is_letter(char c)
    {
        return ((c >= 'a') && (c <= 'z')) || (c >= 'A') && (c <= 'Z');
    }

    private boolean is_blank(char c)
    {
        return ((c == ' ') || (c == '\t')) || (c == '\n') || (c == '\r') ;
    }

    private char proximo()
    {
        int v;
        char x;
        char c;

        if (auxprox != ' ') {
            x = auxprox;
            auxprox=' ';
            return x;
        }

        c = in.le_char();
        return c;
    }

    public CToken pegaToken() 
    {
        int v;
        String s = "";
        CToken w;

        //Elimina brancos.
        if (is_blank(prox)) {
            for (;;prox = proximo()) {
                if (is_blank(prox)) {
                    if (prox == '\n') {
                        linha++;
                    }
                } else {
                    break;
                }
            }
        }

        if (prox == (char)0) {
            return new CToken("fim", "fim");
        }

        //Trata números
        if (is_digit(prox)) {
            v = 0;
            do {
                v = v * 10 + (prox - '0');
                prox = proximo();
            } while (is_digit(prox));
            return new CToken("numero", v + "");
        }

        //Trata identificadores e palavras reservadas
        if (is_letter(prox)) {
            do {
                s = s + prox;
                prox = proximo();
            } while (is_letter(prox) || (is_digit(prox)));

            if (s.equals("read")) {
                return new CToken("read", s);
            }
            if (s.equals("write")) {
                return new CToken("write", s);
            }
            if (s.equals("halt")) {
                return new CToken("halt", s);
            }
            if (s.equals("if")) {
                return new CToken("if", s);
            }
            if (s.equals("else")) {
                return new CToken("else", s);
            }
            if (s.equals("for")) {
                return new CToken("for", s);
            }
            if (s.equals("do")) {
                return new CToken("do", s);
            }
            if (s.equals("while")) {
                return new CToken("while", s);
            }
            if (s.equals("int")) {
                return new CToken("int", s);
            }
            if (s.equals("main")) {
                return new CToken("main", s);
            }
            return new CToken("id", s);
        }

        w = new CToken(prox + "", prox + "");
        prox = ' '; 
        return w;
    }
}
