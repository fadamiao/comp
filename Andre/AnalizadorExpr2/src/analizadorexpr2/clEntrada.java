package analizadorexpr2;

import java.io.*;

public class clEntrada
{
    private File f;
    private FileReader fReader;
    private BufferedReader bReader;
    private StringBuffer fileContent;
    private int charCounter;

    public clEntrada(String url)
    {
        f = new File(url);

        if(!f.exists()) {
            System.exit(0);
        }

        try {
            fReader = new FileReader(f);
            bReader = new BufferedReader(fReader);
            fileContent = new StringBuffer();

            String line = null;
            while ((line = bReader.readLine()) != null){
                fileContent.append(line);
            }

            bReader.close();
            fReader.close();
        } catch (Exception ex) {
            System.out.println("Erro!");
        }       
    }

    public String le_string()
    {
       return fileContent.toString();
    }

    public char le_char()
    {
        char c = fileContent.charAt(charCounter);
        charCounter++;
        return c;
    }

    public void generateEXE(String text)
    {
        if (text != null) {
            try {
                File fFinal = new File(f.getPath() + "\\" + f.getName() + ".exe");
                FileWriter fWriter = new FileWriter(fFinal);

                fWriter.write(text);
                fWriter.close();
            } catch (Exception ex) {
                System.out.println("Um erro ocorreu na hora de salvar o arquivo final!");
            }
        }   
    }
}
