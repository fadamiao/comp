package analizadorexpr2;

import java.io.File;
import java.io.FileWriter;

public class AnalizadorExpr2
{

    public static void main(String[] args)
    {
        clEntrada in = new clEntrada("c:\\temp\\test1.txt");
        clParser P = new clParser(in);

        String progFinal = P.analisa();
        in.generateEXE(progFinal);
    }
}
