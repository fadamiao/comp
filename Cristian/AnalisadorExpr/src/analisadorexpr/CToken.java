/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package analisadorexpr;

/**
 *
 * @author professores
 */
public class CToken {
	String t;
	String val;
	String info;
	
	public CToken(String x, String y) {
		t = x;
		val = y;
		info = "";
	}
        public CToken(String x, String y, String z) {
		t = x;
		val = y;
		info = z;
	}
	
	public boolean igual(String x) {
		return t.equals(x);
	}
	
	public String toString() {
		return t + ":" + val;
	}
        public String info() {
                return info;
        }
        public boolean fim() {
            return t.equals("fim");
        }
}
