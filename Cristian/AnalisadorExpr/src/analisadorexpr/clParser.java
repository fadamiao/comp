/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package analisadorexpr;
import java.util.*;
/**
 *
 * @author professores
 */

 /*
   E ::= T R
   R ::= +T R | ""
   T ::= F Z 
   Z ::= *F Z | "" 
   F ::= a | b | (E) ->  id | (E) | numero
   */

public class clParser {
   private CToken tok;
   private String traducao;
   private clAnalex analex;
   private String comando;
   private int IP;
   
   private Hashtable<String,CToken> Tabela;
   private int indID;
   
   private String expr;
   
   public clParser(clEntrada x) {
       
       analex = new clAnalex(x);
       tok = analex.pegaToken(); 
       traducao = "";
       comando = "";
       expr="";
              
       Tabela = new Hashtable<String,CToken>();
       IP = 0;
       indID = 0;
   }
   
   private int novo_endereco() {
       return indID++;
   }
  
   public String analisa() {
       Boolean k;
       
       k = programa();
       if (k) {
           System.out.println("Programa correto");
           System.out.println(traducao);
           
           return traducao;
       } else {
           System.out.println("Erro de compilação");
           return null;
       }
   }
   
   private boolean Erro(String msg) {
       System.out.println("Erro: " + msg);
       return false;
   }
   
   private boolean programa() {
       boolean k;
       System.out.println("Programa...");
       
       if(match("main") && match("(") && match(")")){
           if(!Bloco()){
               return Erro("Bloco esperado!");
           }
       }
       return true;
       /*traducao="";
       k = comando();
       traducao = traducao + IP + " " + comando + "\n";
       IP++;
       while (!tok.fim() && k) {
           if (tok.igual(";")) {
               match(";");
           } else {
               return Erro("; esperado");
           }
           if (!tok.fim()) {
               k=comando();
               traducao = traducao + IP + " " + comando + "\n";
               IP++;
           }
       }
       return true;*/
   }
   
   private boolean comando() {
       boolean k;
       System.out.println("Comando..." + tok);
       
       comando="";
       if (tok.igual("read")) {
           k = leitura();
           return k;
       }
       if (tok.igual("write")) {
           System.out.println("chamou escrita");
           k = escrita();
           return k;
       }
       if (tok.igual("halt")) {
           comando = "halt";
           match("halt");
           return true;
       }
       if (tok.igual("if")){
           IF();
       }
       if (tok.igual("for")){
           //funcao for
       }
       if (tok.igual("while")){
           //funcao while
       }
       
       k = atrib();
       return k;
   }
   
   private boolean leitura() {
       CToken w; 
       Integer endereco;
       
       comando="set ";
       match("read");
       
       if (!match("(")) return Erro("'(' esperado.");
       
       if (tok.igual("id")) {
           w = Tabela.get(tok.val);
           if (w == null) { // Se não estiver, insere e cria um endereço na memoria para ele.
		endereco = novo_endereco();
                tok.info = endereco.toString();
                Tabela.put(tok.val,tok);
                comando += tok.info;
           } else {
               comando += w.info;
           }
           match("id");
           comando = comando + ",read";
       } else {
           return false;
       }
       
       if (!match(")")) return Erro("')' esperado.");
       
       return true;
   }
   
   private boolean escrita() {
       
       System.out.println("Escrita...");
       
       comando = "set write,";
       
       match("write");
       
       if (!match("(")) return Erro("'(' esperado.");
       
       expr = "";
       
       if (!Expr())         return Erro("Erro na experessão");
       
       comando += expr;
       expr = "";
       
       if (!match(")")) return Erro("')' esperado.");
       
       return true;
   }
   
   private boolean decl() {
       
       CToken w;
       Integer endereco;
       
       if (tok.igual("id")) {
           w = Tabela.get(tok.val);
           if (w == null) {
               System.out.println("Nova variavel em " + indID);
               endereco = novo_endereco();
               tok.info = endereco.toString();
               Tabela.put(tok.val,tok);
               
           } else {
               return Erro("Já existe");
           }
       }
       
       return true;
   }
   
   private boolean atrib() {
       
       System.out.println("Atrib...");
       
       CToken w;
       Integer endereco;
       
       if (tok.igual("id")) {
				// Verifica se o identificador já está na tabela.
           w = Tabela.get(tok.val);
           if (w == null) { 
               return Erro("identificador não encontrado");
               
           } else {
               comando = "set " + w.info + ",";
           }
           match("id");
       } else {
           return Erro("Identificador esperado!");
       }
		
       if (tok.igual("=")) {		
           match("=");
       } else {
           return Erro("'=' esperado.");
       }
       
       expr="";
       
       if (Expr()) {
         comando = comando + expr;
         expr="";
       } else {
           return Erro("Expressão esperada!");
       }
       return true;
   }
   
   private boolean Bloco() {
       if (match("{")) {
           while (comando()) {
               
           }
           if (!match("}")) {
               return Erro("} esperado");
           }
       }
       else {
           if (!comando()) {
               return Erro("Comando esperado");
           }
       }
       
       return true;
   }
   
   public boolean Expr() {
       if (!Termo()) return false;
       if (!R()) return false;
       return true;
   }
   
   public boolean R() {
       if (tok.igual("+")) {
           match("+");
           expr += "+";
           if (!Termo()) return Erro("Erro na expressão 3");
           if (!R()) return false;
           return true;
       }
       if (tok.igual("-")) {
           match("-");
           expr += "-";
           if (!Termo()) return Erro("Erro na expressão 3");
           if (!R()) return false;
           return true;
       }
       
       return true;
   }
   public boolean Termo() {
       if (!Fator()) return Erro("Erro na expressão 2");
       if (!Z()) return false;
       return true;
   } 
   public boolean Z() {
       if (tok.igual("*")) {
           match("*");
           expr += "*";
           if (!Fator()) return Erro("Erro na expressão 1");
           if (!Z()) return false;
           return true;
       }
       if (tok.igual("/")) {
           match("/");
            expr += "/";
           if (!Fator()) return Erro("Erro na expressão 1");
           if (!Z()) return false;
           return true;
       }
       return true;
   }
   
   public boolean Fator() {
       CToken w;
       
       System.out.println(tok);
       if (tok.igual("id")) {
           w = Tabela.get(tok.val);
           if (w == null) { // Se não estiver, insere e cria um endereço na memoria para ele.
               return Erro("Identificador " + tok.val + " desconhecido");
           } else {
               expr += "D[" + w.info + "]";
           }
           match("id");
           return true;
       }
       if (tok.igual("(")) {
            match("(");
            expr += "(";
            Expr();
            if (match(")")) {
                expr += ")";
            } else {
                return Erro("')' esperado.");
            }
            return true;
       }
       if (tok.igual("numero")) {
           expr += tok.val;
           System.out.println("aqui");
           match("numero");
           return true;
       } 
           
       System.out.println("Erro de sintaxe (2)");
       return false;
   }
   
   public boolean match(String x) {
     
       if (tok.igual(x)) {
           tok = analex.pegaToken();
           return true;
       } else {
           return Erro("Erro de sintaxe (3)");
       }
   }
   
   private boolean IF() {
       if (!match("(")) {
           return Erro("( esperado");
       }
       if (!Expr()) {
           return Erro("expressão esperada");
       }
       if (!match(")")) {
           return Erro(") esperado");
       }
       if (!Bloco()) {
           return Erro("Bloco esperado");
       }
       
       if (match("else")) {
           
           if (!Bloco()) {
               return Erro("Bloco esperado");
           }
       }
       
       return true;
   }
   
   private boolean FOR() {
       if (!match("(")) {
           
       }
       
       return true;
   }
   
}
