/**
 *
 * @author Fernando A. Damião <me@fadamiao.com>
 * @license BSD 3-Clause License
 * 
 */

package analisadorexpr;

public class clParser
{
    /*
     * E ::= T R
     * R ::= +T R {print "+"} | ""
     * T ::= F Z
     * Z ::= *F Z {print "*"} | ""
     * F ::= a {print "a"} | b {print "b"}| (E)
     */

    private String S, translate;
    private int pos, size;
    private clAnalex analex;
    private clToken tok;

    public clParser(String x)
    {
        S = x;
        pos = 0;
        size = S.length();

        analex = new clAnalex(S);
        tok = analex.pegaToken();

        translate = "";
    }

    public String analise()
    {
        Expr();

        if (tok != null) {
            System.out.println("Syntax Error: " + tok);
            return "";
        }

        return translate;
    }

    public boolean match(String x) //Exige caractere especificado, pula pro token seguinte
    {
        if (tok == null) {
            showError("Syntax Error: 1");
        }

        if (tok.equal(x)) {
            tok = analex.pegaToken();
        } else {
            showError("Syntax Error: 2");
        }

        return true;
    }

    public String nextSimbol()
    {
        String p;
        pos++;
        if (pos == size) {
            return "";
        }
        p = S.substring(pos, pos + 1);
    
        System.out.println("next = " + p);
        return p;
    }

    public void Factor()
    {
        if (tok == null) {
            showError("Syntax Error: 1");
        }

        if (tok.equal("id")) {
            translate += " " + tok.value;
            match("id");
        } else if (tok.equal("(")) {
            match("(");
            Expr();
            match(")");
        } else if (tok.equal("numero")) {
            translate += " " + tok.value;
            match("numero");
        } else {
            showError("Syntax Error: 1");
        }
    }

    public void Expr()
    {
        Term();
        R();
    }

    public void R()
    {
        if (tok == null) {
            return;
        }

        if (tok.equal("+")) {
            match("+");
            translate += " +";
            Term();
            R();
        }

        if (tok.equal("-")) {
            match("-");
            translate += " -";
            Term();
            R();
        }
    }

    public void Term()
    {
        Factor();
        Z();
    }

    public void Z()
    {
        if (tok == null) {
            return;
        }

        if (tok.equal("*")) {
            match("*");
            translate += " *";
            Factor();
            Z();
        }

        if (tok.equal("/")) {
            match("/");
            translate += " /";
            Factor();
            Z();
        }
    }

    public boolean read() {
        if (!match("read")) {
            return false;
        }
        
        if (!match("(")) {
            return false;
        }

        return true;
    }

    public boolean showError(String s) {
        System.out.println(s);
        return false;
    }
}
