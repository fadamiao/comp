/**
 *
 * @author Fernando A. Damião <me@fadamiao.com>
 * @license BSD 3-Clause License
 * 
 */

package analisadorexpr;

public class AnalisadorExpr
{
    public static void main(String[] args)
    {
        clInput in = new clInput();
        String s = in.leString();

        clParser p = new clParser(s);
        String res = p.analise();

        System.out.println("Result: " + res);
    }
}
