/**
 *
 * @author Fernando A. Damião <me@fadamiao.com>
 * @license BSD 3-Clause License
 * 
 */

package analisadorexpr;

public class clToken
{
    String type;
    String value;

    public clToken(String x, String y)
    {
        type = x;
        value = y;
    }
    
    public boolean equal(String x)
    {
        return type.equals(x);
    }
    
    public String toString() {
        return type + ":" + value;
    }
}
