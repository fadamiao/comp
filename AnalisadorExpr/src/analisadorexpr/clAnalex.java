/**
 *
 * @author Fernando A. Damião <me@fadamiao.com>
 * @license BSD 3-Clause License
 * 
 */

package analisadorexpr;

public class clAnalex
{
    private String prox;
    private String auxprox;
    private String in;
    private int linha;
    private int p;

    public clAnalex(String x) {
    	in = x;
    	linha=0;
        p = 0;
    	auxprox = "";
    	prox = " ";
    }

    private boolean is_digit(char c) {
	return (c >='0') && (c <= '9');
    }

    private boolean is_letter(char c) {
	return ((c >='a') && (c <= 'z')) || ((c >='A') && (c <= 'Z'));
    }

    private boolean is_blank(char c) {
	return ((c ==' ') || (c == '\t')) || ((c =='\n') || (c =='\r')) ;
    }

    private String proximo() {
	int v;
        String x;
	String c;
        
	if (!auxprox.equals("")) {
            x = auxprox;
            auxprox = "";
            return x;
	}

        try {
            c = in.charAt(p) + "";
            p++;
            return c;
	} catch (StringIndexOutOfBoundsException e) {
            return null;
	}
    }

    public clToken pegaToken() {
        int v;
        String s = "";
        clToken w;

        if (prox == null) {
            return null;
        }

        if (is_blank(prox.charAt(0))) {
            for (;;prox = proximo()) {
                if (prox == null) {
                    return null;
                }	
                if ((prox.charAt(0) == ' ') || (prox.charAt(0) == '\t')) {		
                } else if (prox.charAt(0) == '\n') {
                    linha++;
                } else {
                    break;
                }	
            }
        }

        if (is_digit(prox.charAt(0))) {
            v = 0;
            do {
                v = v * 10 + Integer.parseInt(prox);
                prox = proximo();
                if (prox == null) {
                    break;
                }
            } while (is_digit(prox.charAt(0))) ;		
            return new clToken("numero", v + "");
        }

        if (is_letter(prox.charAt(0))) {
            do {
                s = s + prox;
                prox = proximo();
                if (prox == null) {
                    break;
                }
            } while (is_letter(prox.charAt(0)) || (is_digit(prox.charAt(0))));
            return new clToken("id",s);    
        }

        w = new clToken(prox, prox);
        prox = " ";
        return w;
    }  
}
